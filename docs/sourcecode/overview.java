 public interface BusinessCalendar {
       LocalDate plusWorkingDays(LocalDate d, int workingDays);
       int countWorkingDays(LocalDate fromDate, DayEnd fromEnd, LocalDate toDate, DayEnd toEnd);
       boolean isWorkingDay(LocalDate d);
   }
   